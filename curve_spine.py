######################
# Curve Spine Setup
#
# Alan Weider
# alanweider@gmail.com
# 2017
#
######################

from pymel.core import *

def get_help_cb():
    '''
    TODO: Pop up a little helper message to guide the user
    '''
    pass

def create_fit_cb(mode, prefix):
    '''
    Create initial fit structure
    
    Top <- TopHandle
    Mid
    Bot <- BotHandle

    :param mode: RadioGrp from UI indicating whether this is biped/quad
    :type mode: `pymel.core.uiTypes.radioButtonGrp`

    :param prefix: TextField with the desired system prefix name
    :type prefix: `pyeml.core.uiTypes.textField`
    '''
    COLOR_CODES = {"top": 17, # Yellow
                   "bot": 14, # Cyan
                   "mid": 18} # Green
    
    # Create container group
    prefix = prefix.getText() # I don't fully understand why this can't be passed from the Callback yet
    try:
        container_grp = PyNode("{prefix}_grp".format(prefix=prefix))        
    except MayaObjectError:
        container_grp = group(empty=True, name="{prefix}_grp".format(prefix=prefix))

    # (Re)Create fit group if it doesn't exist
    children = container_grp.getChildren(type="transform")
    for child in children:
        if "fit_grp" in child.nodeName():                        
            delete(child)            

    fit_grp = group(empty=True, parent=container_grp, name='fit_grp')   

    # Generate fit structure
    mode = mode.getSelect() # I don't fully understand why this can't be passed from the Callback yet
    fit = {}    
    if mode == 1: # Biped Y-Up
        select(clear=True)
        fit["top_fit"] = joint(name="top_fit", position=[0, 3, 0])
        select(clear=True)
        fit["top_handle_fit"] = joint(name="top_handle_fit", position=[0, 2, 0])
        select(clear=True)
        fit["mid_fit"] = joint(name="mid_fit", position=[0, 0, 0])
        select(clear=True)
        fit["bot_fit"] = joint(name="bot_fit", position=[0, -3, 0])
        select(clear=True)
        fit["bot_handle_fit"] = joint(name="bot_handle_fit", position=[0, -2, 0])
        select(clear=True)
    else: # Quad Z-Up
        select(clear=True)
        fit["top_fit"] = joint(name="top_fit", position=[0, 0, 3])
        select(clear=True)
        fit["top_handle_fit"] = joint(name="top_handle_fit", position=[0, 0, 2])
        select(clear=True)
        fit["mid_fit"] = joint(name="mid_fit", position=[0, 0, 0])
        select(clear=True)
        fit["bot_fit"] = joint(name="bot_fit", position=[0, 0, -3])
        select(clear=True)
        fit["bot_handle_fit"] = joint(name="bot_handle_fit", position=[0, 0, -2])
        select(clear=True)
    
    # Prep hierarchy
    for jnt in fit.values():
        jnt.setParent(fit_grp)
        jnt.scale.lock()
        if "handle" in jnt.nodeName():
            jnt.rotate.lock()
            jnt.translateX.lock()
            if mode == 1:
                jnt.translateZ.lock()
            else:
                jnt.translateY.lock()
       
    fit["top_handle_fit"].setParent(fit["top_fit"])
    fit["bot_handle_fit"].setParent(fit["bot_fit"])
    
    # Color things
    color_override(fit["top_fit"], COLOR_CODES["top"])
    color_override(fit["top_handle_fit"], COLOR_CODES["top"])
    color_override(fit["mid_fit"], COLOR_CODES["mid"])
    color_override(fit["bot_fit"], COLOR_CODES["bot"])
    color_override(fit["bot_handle_fit"], COLOR_CODES["bot"])
    
    # Set some handy labels
    for jnt in fit.values():
        jnt.drawLabel.set(True)
        PyNode(jnt.name() + ".type").set(18) # This is weird, but sets labeling to "other"
        PyNode(jnt.name() + ".otherType").set(jnt.nodeName()) # Is there an easier way to access this?
    
def create_curve_spine_cb(num_joints, prefix):
    '''
    Create a control and joint structure for the curve spine.

    In a nutshell, we create a curve that is deformed by a control joint structure.
    This curve will serve as a motion path for N number of spine joints.  A duplicate
    curve (also deformed by the control structure), will serve as a motion path for up-objects
    to keep the system from flipping as it twists.

    :param num_joints: IntField with the desired number of system joints
    :type num_joints: `pymel.core.uiTypes.intField`

    :param prefix: TextField with the desired system prefix name
    :type prefix: `pyeml.core.uiTypes.textField`
    '''
    
    num_joints = num_joints.getValue()[0]    

    def fit_error(obj_name):
        '''
        '''
        error("Can't find {obj}.  Did it get deleted?".format(obj=obj_name))
    
    # Ensure fit structure exists
    prefix = prefix.getText() # I don't fully understand why this can't be passed from the Callback yet
    try:
        container_grp = PyNode("{prefix}_grp".format(prefix=prefix))
    except MayaObjectError:
        fit_error("{prefix}_grp".format(prefix=prefix))
        return

    children = container_grp.getChildren(allDescendents=True)
    fit_names = ["fit_grp", "top_fit", "top_handle_fit", "mid_fit", "bot_fit", "bot_handle_fit"]
    for name in fit_names:
        match = False
        for child in children:
            if name in child.nodeName():
                match = True
                break
        if not match:
            fit_error(name)
            return
    
    # Prep
    children = container_grp.getChildren(type="transform")
    for child in children:
        if "fit_grp" in child.nodeName():
            fit_grp = child            
        elif "ctrl_grp" in child.nodeName(): # Clear out any old control group contents            
            delete(child)
  
    ctrl_grp = duplicate(fit_grp, name="ctrl_grp")[0]        
    
    ctrl_jnts = {}
    for jnt in ctrl_grp.getChildren(allDescendents=True, type="joint"):
        jnt.rename(jnt.nodeName().replace("fit", "jnt"))
        PyNode(jnt.name() + ".otherType").set(jnt.nodeName())
        ctrl_jnts[jnt.nodeName()] = jnt
    
    # Create control curve
    ctrl_curve = create_control_curve([ctrl_jnts["top_jnt"],
                                       ctrl_jnts["top_handle_jnt"],
                                       ctrl_jnts["mid_jnt"],
                                       ctrl_jnts["bot_handle_jnt"],
                                       ctrl_jnts["bot_jnt"]])

    ctrl_curve.setParent(ctrl_grp)    
    ctrl_curve.inheritsTransform.set(False) # Don't want double transforms!
 
    # Duplicate curve and offset
    up_curve = duplicate(ctrl_curve, name=ctrl_curve.name()+"up")[0]
    up_curve.setTranslation((1, 0, 0)) # TODO: Make this offset axis user defined
    
    # Skin curves to ctrl joints
    for spine_curve in [ctrl_curve, up_curve]:
        animation.skinCluster(ctrl_jnts["top_jnt"],
                              ctrl_jnts["top_handle_jnt"],
                              ctrl_jnts["mid_jnt"],
                              ctrl_jnts["bot_handle_jnt"],
                              ctrl_jnts["bot_jnt"],
                              spine_curve,
                              name="spineCtrlCurveSkinCluster")
    
    # Prepare motion path joints
    ctrl_curve_jnt_grp = group(empty=True, name="ctrl_curve_jnt_grp")
    ctrl_curve_jnt_grp.setParent(ctrl_grp)
    ctrl_curve_jnt_grp.inheritsTransform.set(False)
    ctrl_curve_up_grp = group(empty=True, name="ctrl_curve_up_grp")
    ctrl_curve_up_grp.setParent(ctrl_grp)
    ctrl_curve_up_grp.inheritsTransform.set(False)
    
    mp_ctrl_jnts = create_motion_path_jnts(num_joints, "mp_jnt_spine")
    for jnt in mp_ctrl_jnts:
        jnt.setParent(ctrl_curve_jnt_grp)
        addAttr(shortName="skin", longName="skinJoint")
    mp_up_jnts = create_motion_path_jnts(num_joints, "mp_jnt_up")
    for jnt in mp_up_jnts:
        jnt.setParent(ctrl_curve_up_grp)
   
    # Apply motion path
    ctrl_mp_nodes = apply_motion_path(mp_ctrl_jnts, ctrl_curve, prefix)
    apply_motion_path(mp_up_jnts, up_curve, prefix)
    
    # Set up objects
    for i, jnt in enumerate(mp_ctrl_jnts):
        ctrl_mp_nodes[i].worldUpType.set(1) # Object Up        
        mp_up_jnts[i].xformMatrix.connect(ctrl_mp_nodes[i].worldUpMatrix)

    # Cleanup
    fit_grp.visibility.set(False)
    ctrl_curve_jnt_grp.visibility.set(False)
    ctrl_curve_up_grp.visibility.set(False)
    ctrl_curve.visibility.set(False)
    up_curve.visibility.set(False)
       
def apply_motion_path(jnt_list, mp_curve, prefix):
    '''
    For each joint in the supplied joint list, evenly place them on a curve as a motion path
    
    :paramm jnt_list: List of joints
    :type jnt_list: `[pymel.core.nodetypes.Joint]`
    
    :param mp_curve: Curve to apply joints to as motion path
    :type mp_curve: `pymel.core.nodetypes.Curve`

    :param prefix: String to attempt to ensure uniqueness
    :type prefix: `str`

    :return: List of motion path nodes created
    :rtype: `[pymel.core.nodeTypes.PathAnimation]`
    '''
    mp_nodes = []

    for i, jnt in enumerate(jnt_list):
        mp = PyNode(animation.pathAnimation(jnt, curve=mp_curve, 
                                            name="{prefix}_{jnt}_mp".format(prefix=prefix, jnt=jnt.nodeName()),
                                            follow=True, followAxis="Y", upAxis="X", 
                                            fractionMode=True))
        # Delete auto-created uvalue animation
        upstream = mp.uValue.connections()
        for node in upstream:
            delete(node)     
        # Distribute along curve
        mp.uValue.set(i/float((len(jnt_list) - 1)))
        mp_nodes.append(mp)

    return mp_nodes

def create_motion_path_jnts(num_joints, prefix):
    '''
    Blast out a bunch of joints at the origin with the given prefix
    
    :param num_joints: Number of joints to create
    :type num_joints: `int`
    
    :param prefix: Prefix to prepend joint names with
    :type prefix: `str`
    
    :return: List of joints
    :rtype: `[pymel.core.nodetypes.Joint]`
    '''
    jnt_list = []
    for i in xrange(num_joints):        
        jnt_list.append(nodetypes.Joint(name=prefix + str(i)))
    
    return(jnt_list)
        
def create_control_curve(jnt_list):
    '''
    From a list of joints, create a curve from their positions
    
    :param jnt_list: List of joints
    :type jnt_list: `list pymel.core.nodetypes.Joint`
    
    :return: Resulting curve
    :rtype: `pymel.core.nodetypes.Curve`
    '''                                       
    # Create curve through new control joints
    curve_pos = []
    for jnt in jnt_list:
        curve_pos.append(jnt.getTranslation(space="world"))
    
    ctrl_curve = modeling.curve(name="ctrl_curve", point=curve_pos)
    
    return(ctrl_curve)
    
def color_override(obj, color_index):
    '''
    Sets drawing override on and colors based on provided input shape and index value
    
    :param obj: Object to set color on
    :type: `pymel.core.nodeTypes.Shape` or `pymel.core.nodeTypes.Joint`
    
    :param color_index: Color code index on drawing overrides
    :type: `int`
    '''
    # TODO: Make sure to catch if this attribute doesn't exist
    obj.overrideEnabled.set(True)
    obj.overrideColor.set(color_index)
    
def create_curve_spine_window():
    '''
    Open the UI for curve spine creation workflow
    '''
    win_id = "curve_spine"
    
    if window(win_id, exists=True):
        deleteUI(win_id)
 
    with window(win_id, title="Create Curve Spine System", width=150, height=100) as win:
        with columnLayout( rowSpacing=5 ):
            with rowLayout(numberOfColumns=2): # Prefix
                text("Choose a system prefix")
                system_prefix = textField()
            with rowLayout(): # Fit Text
                text("Let's get started!  Start by pressing 'Fit' and place them where you like.")
            with rowLayout(numberOfColumns=3): # Fitting            
                rdio_grp = radioButtonGrp("mode_group", labelArray2=["Biped", "Quad"], 
                                          numberOfRadioButtons=2, 
                                          columnWidth=[1, 60],
                                          select=1)
                button(label="Create Fit", command=Callback(create_fit_cb, rdio_grp, system_prefix))
                #button(label="?", command=Callback(get_help_cb), enable=False)
            with rowLayout():
                separator( width=300, style='in' )
            with rowLayout(): # Create Text
                text("Once your fit joints are where you like them,\nchoose how many joints you'd like and create!")          
            with rowLayout(numberOfColumns=3): # System creation
                num_joints = intFieldGrp(numberOfFields=1, label='Curve Joints', columnAlign2=['left', 'left'], columnWidth2=[60, 50], value1=9)
                button(label="Create System", command=Callback(create_curve_spine_cb, num_joints, system_prefix))
                #button(label="?", command=Callback(get_help_cb), enable=False)
            with rowLayout():
                separator(width=300, style='in')
    win.show()

def curve_spine():
    create_curve_spine_window()

######################
# Curve Spine Setup
#
# Alan Weider
# alanweider@gmail.com
# 2017
#
######################